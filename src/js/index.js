import $ from 'jquery';
import slick from 'slick-carousel';
import { TimalineMax } from 'gsap';

let tl = new TimelineMax(),
    tl2 = new TimelineMax()

tl
    .fromTo('.main-nav__logo', 0.5, { x: -100}, { x: 0 })
    .fromTo('.main-header .main-header__btn', .3, { opacity: 0, y: -50 }, { opacity: 1, y: 0 }, 0)
tl2
    .staggerFrom('.nav a', 0.5, {opacity:0, y:-30, ease:Back.easeIn}, 0.05)

$(document).ready(function() {
    //get active page
    let activePage = $('span[data-page]', 'body').data('page')
    $('li[data-page=' + activePage + ']').addClass('active')

    let mobMenuBtn = $('.mobmenu-btn', '#main-navigation');
    let mobMenuClose = $('.closebtn', '.mobmenu');

    $(mobMenuBtn).click(() => {
        $('.mobmenu').addClass('is-open');
        $('body').addClass('no-scroll');
    })

    $(mobMenuClose).click(() => {
        $('.mobmenu').removeClass('is-open');
        $('body').removeClass('no-scroll');
    })

    $('a', '.mobmenu').click(function() {
        $('.mobmenu').removeClass('is-open');
        $('body').removeClass('no-scroll');
    })

    $('.clients__slider').slick({
        dots: true,
        arrows: false,
        infinity: true,
        slidesToShow: 6,
        slidesToScroll: 3,
        speed: 600,
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 520,
                settings: {
                    slidesToShow: 2,
                    centerMode: false
                }
            },
            {
                breakpoint: 420,
                settings: {
                    slidesToShow: 1,
                    dots: false
                }
            }
        ]
    });

    $(".main-header__mouse").on("click", function(event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;

        $('body, html').animate({ scrollTop: top }, 800);
    });
})
